import { Injectable } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  constructor(private apollo: Apollo) {}

  getCharactersByPage(page: number) {
    return this.apollo.query({
      query: gql`
        query characters($page: Int, $filter: FilterCharacter) {
          characters(page: $page, filter: $filter) {
            info {
              count
              pages
              next
              prev
            }
            results {
              id
              name
              species
              image
              status
            }
          }
        }
      `,
      variables: { page },
    });
  }

  getCharacter(id: string) {
    return this.apollo.query({
      query: gql`
        query character($id: ID!) {
          character(id: $id) {
            gender
            origin {
              name
            }
          }
        }
      `,
      variables: { id },
    });
  }
}

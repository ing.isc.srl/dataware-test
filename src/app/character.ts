export interface Character {
  id: string;
  name: string;
  species: string;
  gender?: string;
  status: string;
  image: string;
  origin?: string;
}

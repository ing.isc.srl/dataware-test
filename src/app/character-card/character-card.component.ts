import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { Character } from '../character';

@Component({
  standalone: true,
  selector: 'app-character-card',
  templateUrl: './character-card.component.html',
  styleUrls: ['./character-card.component.scss'],
  imports: [CommonModule],
})
export class CharacterCardComponent {
  @Input() character!: Character;
}

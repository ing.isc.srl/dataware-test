import { CommonModule } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Character } from '../character';

@Component({
  standalone: true,
  selector: 'app-character-details',
  templateUrl: './character-details.component.html',
  styleUrls: ['./character-details.component.scss'],
  imports: [CommonModule],
})
export class CharacterDetailsComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: Character) {}
}

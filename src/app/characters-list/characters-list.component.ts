import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import {
  MatPaginatorIntl,
  MatPaginatorModule,
  PageEvent,
} from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { AppService } from '../app.service';
import { Character } from '../character';
import { PaginatorIntl } from './paginator-intl';
import { CommonModule } from '@angular/common';
import { CharacterCardComponent } from '../character-card/character-card.component';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { CharacterDetailsComponent } from '../character-details/character-details.component';
import { SpinnerComponent } from '../spinner/spinner.component';

@Component({
  standalone: true,
  selector: 'app-characters-list',
  templateUrl: './characters-list.component.html',
  styleUrls: ['./characters-list.component.scss'],
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    MatPaginatorModule,
    MatInputModule,
    MatSnackBarModule,
    SpinnerComponent,
    CharacterCardComponent,
  ],
  providers: [
    {
      provide: MatPaginatorIntl,
      useClass: PaginatorIntl,
    },
  ],
})
export class CharactersListComponent implements OnInit {
  @ViewChild('pageInput', { static: false }) input!: ElementRef;
  loading = true;
  characters: Character[] = [];
  pageIndex = 0;
  pagination = {
    count: 0,
    pages: 0,
    next: 0,
    prev: 0,
  };

  constructor(
    private appService: AppService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.getCharacters.bind(this)(1);
  }

  getCharacters(page: number) {
    this.appService
      .getCharactersByPage(page)
      .subscribe(this.handleGetCharactersNext.bind(this));
  }

  handleGetCharactersNext(res: any) {
    const data = res.data as any;
    this.loading = res.loading;
    this.pagination = data.characters.info;
    this.characters = data.characters.results;
  }

  page(e: PageEvent) {
    this.loading = true;
    this.getCharacters.bind(this)(e.pageIndex + 1);
  }

  click(character: Character) {
    const dialogSpinner = this.dialog.open(SpinnerComponent, {
      disableClose: false,
    });

    this.appService.getCharacter(character.id).subscribe((res) => {
      const data = res.data as any;
      const {
        gender,
        origin: { name },
      } = data.character;
      const componentData = { data: { ...character, origin: name, gender } };

      this.dialog.open(CharacterDetailsComponent, componentData);
      dialogSpinner.close();
    });
  }

  change(e: Event) {
    this.loading = true;
    const target = e.target as HTMLInputElement;
    const page = parseInt(target.value, 10);
    const pageLimit = this.pagination.pages;
    this.input.nativeElement.value = '';

    if (page > 0 && page <= pageLimit) this.getCharacters.bind(this)(page);
    else {
      const message = `Ingresa una página del 1 al ${pageLimit}`;
      this.snackbar.open(message, '', { duration: 2000 });
    }
  }
}
